# My Resume

[Preview Resume](https://carlosmonti.gitlab.io/resume/)

![sarahconnor](/uploads/e4c6576f10b74301746b623696bd5210/sarahconnor.jpg)

## Download

You can download my resume in two ([ats compatible](https://en.wikipedia.org/wiki/Applicant_tracking_system)) formats

* [PDF](https://gitlab.com/carlosmonti/resume/raw/master/public/carlos-monti-cv.pdf)
* [DOCX](https://gitlab.com/carlosmonti/resume/raw/master/public/carlos-monti-cv.docx)

### Credits

[Tim Baker](https://github.com/tbakerx) and his [react resume template](https://github.com/tbakerx/react-resume-template)
